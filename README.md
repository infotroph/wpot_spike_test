
Investigating a strange interaction in OpenSimRoot between soil bulk density and Doussan water status, possibly moderated by root growth impedance and recently proposed changes in tming of root node creation. Entirely possible this is all just a bad input parameter, but posting it here as an attempted reprex.

Base file:

* Maize with 3D N and Doussan water
* Originally configured for 40 days but reduced to 5 for this demo.
* Bulk density set to constant 1.2 g/cm3
* Other soil and atmosphere parameters taken from Rock Springs environment files
* Includes impedance of root growth by soil hardness, but at 1.2 g/cm3 visible effect is minor.


Treatments:

* Bulk density varied from 1.00 to 1.40 in steps of 0.05 at ends of range, steps of 0.01 from 1.20-1.30
* impedance module left enabled (`with_impedance`) or disabled (`no_impedance`)
	by deleting simulaDerivatives `environment/soil/soilPenetrationResistance`  `dataPointTemplate/rootGrowthImpedance`
* Run with binaries compiled from code that does (`nodetime`) or does not (`current`) include new maximum time criterion for creation of root nodes from `SettingMaxTimeDiffBetweenNodes` branch


Observations:

* All conditions show spikes in calculated water potential during days 2-4
	- If output is daily, no visible spikes in nodetime no_impedance. If 0.1 day interval, see spikes in all.
	- With 0.1 day output interval, no-impedance models have spikes at same times in all bulk densities. models with impedance all have ~same number of spikes, but time shifts depending on bulk density.
* Small changes in bulk density make spikes appear or disappear
* nodetime with impedance has four models that exit in day 4 reporting 
	"FATAL ERROR: pcgSolver: did not find a solution"
* VTP files not included here to keep repository size down; all seemed unremarkable to me, but happy to share for debugging if useful.
