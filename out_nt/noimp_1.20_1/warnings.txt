Simulation took (hours:minutes:seconds): 0:3:49
THE MODEL PUT OUT WARNINGS:
At 5 days mem usage was 247 mB.	1x
ConstantRootGrowth: growthRateMultiplier found	124x
Enabling Probe All Objects for current the model is faster and more accurate if "carbonAllocation2Shoot" and "plantNutrientUptake" is called first. Enable this in parameter section to get rid of this message.	1x
GenerateRoot: using root class specific template for hypocotyl	1x
IntegrationBase:: setting defaultSpatialIntegrationLength to 1.000000.	1x
LeafAreaIndex: using mean leaf area based on areaPerPlant.	1x
LeafAreaReductionCoefficient: Reducing leaf area	238x
OpenSimRoot was build on May  7 2020 from git version f4691b1b5a	1x
PlantCarbonBalance: balance error at +0.10 days: error (%) tot=+2.5187 root=+2.5187 alloc=+0.0000 for maize	1x
PlantCarbonBalance: balance error at +0.30 days: error (%) tot=+0.8012 root=+0.8012 alloc=-0.0000 for maize	1x
PlantTotal: Using initial value for plantNutrientUptake from parameter section	1x
RootDiameter: no lengthMultiplier2DiameterMultiplier specified for hypocotyls of noimp_1.20	2x
RootDiameter: using length - diameter relation. e.g. longer laterals are thicker	248x
RootGrowthDirection: timeScalingFactor not found. Defaulting to 5.	1x
Seeding random number generator with  296864.	1x
SimulaExternal:: locked, and it has no predictor. This is likely to cause extrapolation warnings. 	23527x
SimulaTable:: Extrapolation at end of table hydraulicHeadAtRootSurface	3976x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Leafs. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Shoot. This maybe a poor estimate	28x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Stems. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for carbonAvailableForGrowth. This maybe a poor estimate	12x
SimulaVariable::getRate: estimating initial rate for carbonReserves. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for leafArea. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for plantCarbonIncome. This maybe a poor estimate	10x
SimulaVariable::getRate: estimating initial rate for reserves. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for rootCarbonCostOfNutrientUptake. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for rootCarbonCosts. This maybe a poor estimate	26x
Swms3d:: simulating nitrate	1x
WaterUptakeDoussanModel: Can not find solution incrementally, trying direct interpolation.	2x
WaterUptakeDoussanModel: Ignoring node with name growthpoint	122x
WaterUptakeDoussanModel: Root system can not take up enough water at day 2	14x
WaterUptakeDoussanModel: Root system can not take up enough water at day 3	15x
WaterUptakeDoussanModel: Root system can not take up enough water at day 4	8x
WaterUptakeDoussanModel::build: getting low h(soil), setting lr to 0. Root outside grid?	1775x
WaterUptakeDoussanModel::build: using same matrix	463x
Watflow: Using Free drainage bottom boundary.	1x
Watflow:: using evaporation from environment section, i.e. given, predefined.	1x
Watflow::Surface area water mesh is 484	1x
