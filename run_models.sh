#!/bin/bash


cd ${1:-'output'}
# export OSR_BIN=${2:-"${HOME}/OSR_bin_archive/OSR_f4691b1b5a_imped_nodetime_gcc7"}
export OSR_BIN=${2:-"${HOME}/OSR_bin_archive/OSR_67fdb3eda1_imped_gcc7"}

function run_osr {
	cd $(dirname ${1}) && ${OSR_BIN} -v -ww $(basename ${1}) > runlog.txt 2>&1
}
export -f run_osr

parallel \
	-j4 --memfree 400M --delay 6 --load 90% \
	--resume-failed --retries 5 --joblog runlog_parallel.txt \
	run_osr {} < model_files.txt
