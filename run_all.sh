#!/bin/bash


## Paths to binaries: Edit to match your system.
#
# built from commit 67fdb3eda1,
# which is current (2020-05-11) tip of infotroph's `impedance` branch
CURRENT_BIN=${HOME}/OSR_bin_archive/OSR_67fdb3eda1_imped_gcc7 
#
# built from f4691b1b5a, which is made by merging
# 67fdb3eda1 (tip of infotroph's `impedance` branch)
# into 6f8e57d (current tip of `SettingMaxTimeDiffBetweenNodes` branch)
NODETIME_BIN=${HOME}/OSR_bin_archive/OSR_f4691b1b5a_imped_nodetime_gcc7

## Create run files
# needs `simrootR` package. To install:
# Rscript -e 'remotes::install_gitlab("LynchLab/simrootR@latest")'
./set_up_models.R out_nt 1 
./set_up_models.R out_cur 1 

## Run models
# needs Gnu parallel, assumes 4 cores available
# adjust for your system in run_models.sh
./run_models.sh out_cur ${CURRENT_BIN}
./run_models.sh out_nt ${NODETIME_BIN}

## plot plant water potential across first 5 days
# Note spikes to VERY dry conditions,
# large changes in water potential from small change in bulk density, 
# model failures in day 4 (with "pcgSolver: did not find a solution"),
./plot.R
