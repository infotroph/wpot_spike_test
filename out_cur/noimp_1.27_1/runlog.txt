[30;47mRunning OpenSimRoot 
	build on May  7 2020, 
	licensed GPLv3 
	git 67fdb3eda1
[34;47mOpenSimRoot was build on May  7 2020 from git version 67fdb3eda1[30;47m

Trying to load model from file: [32;47m[800C[1DOK[30;47m
[34;47mSeeding random number generator with  223640.[30;47m
[30;47m
Running modules: [30;47mRunning modules: 
[34;47mEnabling Probe All Objects for current the model is faster and more accurate if "carbonAllocation2Shoot" and "plantNutrientUptake" is called first. Enable this in parameter section to get rid of this message.[30;47m

[34;47mGenerateRoot: using root class specific template for hypocotyl[30;47m

[34;47mRootGrowthDirection: timeScalingFactor not found. Defaulting to 5.[30;47m

[34;47mIntegrationBase:: setting defaultSpatialIntegrationLength to 1.000000.[30;47m

[34;47mLeafAreaIndex: using mean leaf area based on areaPerPlant.[30;47m

[34;47mConstantRootGrowth: growthRateMultiplier found[30;47m

[34;47mRootDiameter: using length - diameter relation. e.g. longer laterals are thicker[30;47m

[34;47mRootDiameter: no lengthMultiplier2DiameterMultiplier specified for hypocotyls of noimp_1.27[30;47m

[34;47mPlantTotal: Using initial value for plantNutrientUptake from parameter section[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for carbonAvailableForGrowth. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for carbonAllocation2Shoot. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for plantCarbonIncome. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for rootCarbonCosts. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for carbonReserves. This maybe a poor estimate[30;47m

[34;47mWatflow:: using evaporation from environment section, i.e. given, predefined.[30;47m

[34;47mWatflow: Using Free drainage bottom boundary.[30;47m

[34;47mWatflow::Surface area water mesh is 484[30;47m

[34;47mSwms3d:: simulating nitrate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for carbonAllocation2Leafs. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for carbonAllocation2Stems. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for leafArea. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for reserves. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for rootCarbonCostOfNutrientUptake. This maybe a poor estimate[30;47m
[s0.0/5.0 days. Mem 235.0 mB. #obj.=1883 x64b/obj.=16357.9[u
[34;47mWaterUptakeDoussanModel::build: using same matrix[30;47m

[34;47mSimulaExternal:: locked, and it has no predictor. This is likely to cause extrapolation warnings. [30;47m

[34;47mPlantCarbonBalance: balance error at +0.10 days: error (%) tot=+2.5187 root=+2.5187 alloc=+0.0000 for maize[30;47m
[s0.1/5.0 days. Mem 238.0 mB. #obj.=1883 x64b/obj.=16566.7[u
[34;47mSimulaTable:: Extrapolation at end of table hydraulicHeadAtRootSurface[30;47m
[s0.2/5.0 days. Mem 238.0 mB. #obj.=2098 x64b/obj.=14869.0[u
[34;47mPlantCarbonBalance: balance error at +0.30 days: error (%) tot=+0.8012 root=+0.8012 alloc=-0.0000 for maize[30;47m
[s0.3/5.0 days. Mem 238.0 mB. #obj.=2098 x64b/obj.=14869.0[u
[34;47mWaterUptakeDoussanModel: Ignoring node with name growthpoint[30;47m
[s0.4/5.0 days. Mem 239.0 mB. #obj.=2734 x64b/obj.=11458.0[u[s0.5/5.0 days. Mem 239.0 mB. #obj.=2734 x64b/obj.=11458.0[u[s0.6/5.0 days. Mem 239.0 mB. #obj.=2863 x64b/obj.=10941.7[u[s0.7/5.0 days. Mem 239.0 mB. #obj.=2863 x64b/obj.=10941.7[u[s0.8/5.0 days. Mem 239.0 mB. #obj.=2949 x64b/obj.=10622.7[u[s0.9/5.0 days. Mem 239.0 mB. #obj.=2949 x64b/obj.=10622.7[u[s1.0/5.0 days. Mem 239.0 mB. #obj.=2949 x64b/obj.=10622.7[u[s1.0/5.0 days. Mem 239.0 mB. #obj.=3035 x64b/obj.=10321.7[u[s1.1/5.0 days. Mem 239.0 mB. #obj.=3035 x64b/obj.=10321.7[u
[34;47mWaterUptakeDoussanModel::build: getting low h(soil), setting lr to 0. Root outside grid?[30;47m
[s1.2/5.0 days. Mem 239.0 mB. #obj.=3121 x64b/obj.=10037.2[u[s1.3/5.0 days. Mem 239.0 mB. #obj.=3121 x64b/obj.=10037.2[u[s1.4/5.0 days. Mem 240.0 mB. #obj.=3207 x64b/obj.=9808.9[u[s1.5/5.0 days. Mem 240.0 mB. #obj.=3207 x64b/obj.=9808.9[u[s1.6/5.0 days. Mem 240.0 mB. #obj.=3336 x64b/obj.=9429.6[u[s1.7/5.0 days. Mem 240.0 mB. #obj.=3336 x64b/obj.=9429.6[u[s1.8/5.0 days. Mem 240.0 mB. #obj.=3465 x64b/obj.=9078.6[u[s1.9/5.0 days. Mem 240.0 mB. #obj.=3465 x64b/obj.=9078.6[u[s2.0/5.0 days. Mem 240.0 mB. #obj.=3465 x64b/obj.=9078.6[u
[34;47mWaterUptakeDoussanModel: Root system can not take up enough water at day 2[30;47m

[34;47mLeafAreaReductionCoefficient: Reducing leaf area[30;47m
[s2.0/5.0 days. Mem 240.0 mB. #obj.=3900 x64b/obj.=8066.0[u[s2.1/5.0 days. Mem 240.0 mB. #obj.=3900 x64b/obj.=8066.0[u
[34;47mWaterUptakeDoussanModel: Can not find solution incrementally, trying direct interpolation.[30;47m
[s2.2/5.0 days. Mem 240.0 mB. #obj.=4617 x64b/obj.=6813.4[u[s2.3/5.0 days. Mem 240.0 mB. #obj.=4617 x64b/obj.=6813.4[u[s2.4/5.0 days. Mem 240.0 mB. #obj.=5248 x64b/obj.=5994.1[u[s2.5/5.0 days. Mem 240.0 mB. #obj.=5248 x64b/obj.=5994.1[u[s2.6/5.0 days. Mem 241.0 mB. #obj.=6051 x64b/obj.=5220.4[u[s2.7/5.0 days. Mem 241.0 mB. #obj.=6051 x64b/obj.=5220.4[u
[34;47mWaterUptakeDoussanModel: Root system can not take up enough water at day 3[30;47m
[s2.8/5.0 days. Mem 241.0 mB. #obj.=6701 x64b/obj.=4714.0[u[s2.9/5.0 days. Mem 241.0 mB. #obj.=6701 x64b/obj.=4714.0[u[s3.0/5.0 days. Mem 241.0 mB. #obj.=6701 x64b/obj.=4714.0[u[s3.0/5.0 days. Mem 241.0 mB. #obj.=7418 x64b/obj.=4258.3[u[s3.1/5.0 days. Mem 241.0 mB. #obj.=7418 x64b/obj.=4258.3[u[s3.2/5.0 days. Mem 242.0 mB. #obj.=8025 x64b/obj.=3952.6[u[s3.3/5.0 days. Mem 242.0 mB. #obj.=8025 x64b/obj.=3952.6[u[s3.4/5.0 days. Mem 242.0 mB. #obj.=8742 x64b/obj.=3628.4[u[s3.5/5.0 days. Mem 242.0 mB. #obj.=8742 x64b/obj.=3628.4[u[s3.6/5.0 days. Mem 242.0 mB. #obj.=9349 x64b/obj.=3392.8[u[s3.7/5.0 days. Mem 242.0 mB. #obj.=9349 x64b/obj.=3392.8[u
[34;47mWaterUptakeDoussanModel: Root system can not take up enough water at day 4[30;47m
[s3.8/5.0 days. Mem 243.0 mB. #obj.=10152 x64b/obj.=3137.4[u[s3.9/5.0 days. Mem 243.0 mB. #obj.=10152 x64b/obj.=3137.4[u[s4.0/5.0 days. Mem 243.0 mB. #obj.=10152 x64b/obj.=3137.4[u[s4.0/5.0 days. Mem 243.0 mB. #obj.=11022 x64b/obj.=2889.7[u[s4.1/5.0 days. Mem 243.0 mB. #obj.=11022 x64b/obj.=2889.7[u[s4.2/5.0 days. Mem 244.0 mB. #obj.=13346 x64b/obj.=2396.3[u[s4.3/5.0 days. Mem 244.0 mB. #obj.=13346 x64b/obj.=2396.3[u[s4.4/5.0 days. Mem 245.0 mB. #obj.=16373 x64b/obj.=1961.3[u[s4.5/5.0 days. Mem 245.0 mB. #obj.=16373 x64b/obj.=1961.3[u[s4.6/5.0 days. Mem 246.0 mB. #obj.=20060 x64b/obj.=1607.4[u[s4.7/5.0 days. Mem 246.0 mB. #obj.=20060 x64b/obj.=1607.4[u[s4.8/5.0 days. Mem 248.0 mB. #obj.=23790 x64b/obj.=1366.4[u[s4.9/5.0 days. Mem 248.0 mB. #obj.=23790 x64b/obj.=1366.4[u[s5.0/5.0 days. Mem 248.0 mB. #obj.=23790 x64b/obj.=1366.4[u
[34;47mAt 5 days mem usage was 248 mB.[30;47m
[32;47m[800C[1DOK[30;47m
Finalizing output:[32;47m[800C[1DOK[30;47m[34;47m
THE MODEL PUT OUT WARNINGS:
At 5 days mem usage was 248 mB.[800C[7D1x
ConstantRootGrowth: growthRateMultiplier found[800C[7D139x
Enabling Probe All Objects for current the model is faster and more accurate if "carbonAllocation2Shoot" and "plantNutrientUptake" is called first. Enable this in parameter section to get rid of this message.[800C[7D1x
GenerateRoot: using root class specific template for hypocotyl[800C[7D1x
IntegrationBase:: setting defaultSpatialIntegrationLength to 1.000000.[800C[7D1x
LeafAreaIndex: using mean leaf area based on areaPerPlant.[800C[7D1x
LeafAreaReductionCoefficient: Reducing leaf area[800C[7D238x
OpenSimRoot was build on May  7 2020 from git version 67fdb3eda1[800C[7D1x
PlantCarbonBalance: balance error at +0.10 days: error (%) tot=+2.5187 root=+2.5187 alloc=+0.0000 for maize[800C[7D1x
PlantCarbonBalance: balance error at +0.30 days: error (%) tot=+0.8012 root=+0.8012 alloc=-0.0000 for maize[800C[7D1x
PlantTotal: Using initial value for plantNutrientUptake from parameter section[800C[7D1x
RootDiameter: no lengthMultiplier2DiameterMultiplier specified for hypocotyls of noimp_1.27[800C[7D2x
RootDiameter: using length - diameter relation. e.g. longer laterals are thicker[800C[7D278x
RootGrowthDirection: timeScalingFactor not found. Defaulting to 5.[800C[7D1x
Seeding random number generator with  223640.[800C[7D1x
SimulaExternal:: locked, and it has no predictor. This is likely to cause extrapolation warnings. [800C[7D23792x
SimulaTable:: Extrapolation at end of table hydraulicHeadAtRootSurface[800C[7D4183x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Leafs. This maybe a poor estimate[800C[7D4x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Shoot. This maybe a poor estimate[800C[7D28x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Stems. This maybe a poor estimate[800C[7D4x
SimulaVariable::getRate: estimating initial rate for carbonAvailableForGrowth. This maybe a poor estimate[800C[7D12x
SimulaVariable::getRate: estimating initial rate for carbonReserves. This maybe a poor estimate[800C[7D4x
SimulaVariable::getRate: estimating initial rate for leafArea. This maybe a poor estimate[800C[7D4x
SimulaVariable::getRate: estimating initial rate for plantCarbonIncome. This maybe a poor estimate[800C[7D10x
SimulaVariable::getRate: estimating initial rate for reserves. This maybe a poor estimate[800C[7D4x
SimulaVariable::getRate: estimating initial rate for rootCarbonCostOfNutrientUptake. This maybe a poor estimate[800C[7D4x
SimulaVariable::getRate: estimating initial rate for rootCarbonCosts. This maybe a poor estimate[800C[7D26x
Swms3d:: simulating nitrate[800C[7D1x
WaterUptakeDoussanModel: Can not find solution incrementally, trying direct interpolation.[800C[7D6x
WaterUptakeDoussanModel: Ignoring node with name growthpoint[800C[7D137x
WaterUptakeDoussanModel: Root system can not take up enough water at day 2[800C[7D15x
WaterUptakeDoussanModel: Root system can not take up enough water at day 3[800C[7D15x
WaterUptakeDoussanModel: Root system can not take up enough water at day 4[800C[7D9x
WaterUptakeDoussanModel::build: getting low h(soil), setting lr to 0. Root outside grid?[800C[7D1811x
WaterUptakeDoussanModel::build: using same matrix[800C[7D463x
Watflow: Using Free drainage bottom boundary.[800C[7D1x
Watflow:: using evaporation from environment section, i.e. given, predefined.[800C[7D1x
Watflow::Surface area water mesh is 484[800C[7D1x[30;47m
Simulation took (hours:minutes:seconds): 0:3:48[0m
