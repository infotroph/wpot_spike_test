Simulation took (hours:minutes:seconds): 0:4:9
THE MODEL PUT OUT WARNINGS:
At 5 days mem usage was 243 mB.	1x
ConstantRootGrowth: growthRateMultiplier found	20x
Enabling Probe All Objects for current the model is faster and more accurate if "carbonAllocation2Shoot" and "plantNutrientUptake" is called first. Enable this in parameter section to get rid of this message.	1x
GenerateRoot: using root class specific template for hypocotyl	1x
ImpedanceGao: no call pointer passed so can't look up water content. Returning 0	51x
IntegrationBase:: setting defaultSpatialIntegrationLength to 1.000000.	1x
LeafAreaIndex: using mean leaf area based on areaPerPlant.	1x
OpenSimRoot was build on May  7 2020 from git version 67fdb3eda1	1x
PlantCarbonBalance: balance error at +0.10 days: error (%) tot=+2.5297 root=+2.5297 alloc=+0.0000 for maize	1x
PlantCarbonBalance: balance error at +0.30 days: error (%) tot=+0.8025 root=+0.8025 alloc=+0.0000 for maize	1x
PlantTotal: Using initial value for plantNutrientUptake from parameter section	1x
RootDataPoints: refusing to create points based on estimated data, but the data is before t.	565x
RootDiameter: no lengthMultiplier2DiameterMultiplier specified for hypocotyls of imp_1.40	2x
RootDiameter: using length - diameter relation. e.g. longer laterals are thicker	40x
RootGrowthDirection: timeScalingFactor not found. Defaulting to 5.	1x
Seeding random number generator with  559813.	1x
SimulaExternal:: locked, and it has no predictor. This is likely to cause extrapolation warnings. 	7038x
SimulaTable:: Extrapolation at end of table rootSegmentWaterUptakeRate	12707x
SimulaTable:: Extrapolation at end of table volumetricWaterContentAtTheRootSurface	10x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Leafs. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Shoot. This maybe a poor estimate	28x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Stems. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for carbonAvailableForGrowth. This maybe a poor estimate	82x
SimulaVariable::getRate: estimating initial rate for carbonReserves. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for leafArea. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for plantCarbonIncome. This maybe a poor estimate	10x
SimulaVariable::getRate: estimating initial rate for reserves. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for rootCarbonCostOfNutrientUptake. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for rootCarbonCosts. This maybe a poor estimate	26x
SoluteMassBalanceTest: uptake may not equal sink for nitrate. Message could be caused by seed reserves added to simulation after t=0, or roots outside the grid etc, but please check	30x
Swms3d:: simulating nitrate	1x
Swms3d::getTimeStepSWMS:: wallTime before lastTime, this is bad - ignoring wall time.	39x
WaterUptakeDoussanModel: Ignoring node with name growthpoint	18x
WaterUptakeDoussanModel: Root system can not take up enough water at day 2	6x
WaterUptakeDoussanModel: Root system can not take up enough water at day 3	4x
WaterUptakeDoussanModel::build: getting low h(soil), setting lr to 0. Root outside grid?	308x
WaterUptakeDoussanModel::build: using same matrix	263x
Watflow: Using Free drainage bottom boundary.	1x
Watflow:: using evaporation from environment section, i.e. given, predefined.	1x
Watflow::Surface area water mesh is 484	1x
