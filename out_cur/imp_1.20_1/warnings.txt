Simulation took (hours:minutes:seconds): 0:3:59
THE MODEL PUT OUT WARNINGS:
At 5 days mem usage was 247 mB.	1x
ConstantRootGrowth: growthRateMultiplier found	137x
Enabling Probe All Objects for current the model is faster and more accurate if "carbonAllocation2Shoot" and "plantNutrientUptake" is called first. Enable this in parameter section to get rid of this message.	1x
GenerateRoot: using root class specific template for hypocotyl	1x
ImpedanceGao: Soil physical and hydraulic parameters are inconsistent. Water content at saturation differs more than 10% from porosity implied by bulk density	696x
ImpedanceGao: no call pointer passed so can't look up water content. Returning 0	51x
IntegrationBase:: setting defaultSpatialIntegrationLength to 1.000000.	1x
LeafAreaIndex: using mean leaf area based on areaPerPlant.	1x
LeafAreaReductionCoefficient: Reducing leaf area	210x
OpenSimRoot was build on May  7 2020 from git version 67fdb3eda1	1x
PlantCarbonBalance: balance error at +0.10 days: error (%) tot=+2.5243 root=+2.5243 alloc=+0.0000 for maize	1x
PlantCarbonBalance: balance error at +0.30 days: error (%) tot=+0.8022 root=+0.8022 alloc=+0.0000 for maize	1x
PlantCarbonBalance: balance error at +2.10 days: error (%) tot=+0.5785 root=+0.5762 alloc=+0.0000 for maize	1x
PlantCarbonBalance: balance error at +2.30 days: error (%) tot=+0.6848 root=+0.6806 alloc=+0.0000 for maize	1x
PlantCarbonBalance: balance error at +2.50 days: error (%) tot=+0.5122 root=+0.5071 alloc=+0.0000 for maize	1x
PlantTotal: Using initial value for plantNutrientUptake from parameter section	1x
RootDataPoints: refusing to create points based on estimated data, but the data is before t.	15910x
RootDiameter: no lengthMultiplier2DiameterMultiplier specified for hypocotyls of imp_1.20	2x
RootDiameter: using length - diameter relation. e.g. longer laterals are thicker	274x
RootGrowthDirection: timeScalingFactor not found. Defaulting to 5.	1x
RootSegmentLength: length based on estimated code, but this should not be the case as nodes are not created based on estimates.	19x
RootSegmentLength: length based on estimated code, but this should not be the case as nodes are not created based on estimates. (2)	19x
Seeding random number generator with  832310.	1x
SimulaExternal:: locked, and it has no predictor. This is likely to cause extrapolation warnings. 	63697x
SimulaTable:: Extrapolation at end of table hydraulicHeadAtRootSurface	3029x
SimulaTable:: Extrapolation at end of table volumetricWaterContentAtTheRootSurface	32277x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Leafs. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Shoot. This maybe a poor estimate	28x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Stems. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for carbonAvailableForGrowth. This maybe a poor estimate	82x
SimulaVariable::getRate: estimating initial rate for carbonReserves. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for leafArea. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for plantCarbonIncome. This maybe a poor estimate	10x
SimulaVariable::getRate: estimating initial rate for reserves. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for rootCarbonCostOfNutrientUptake. This maybe a poor estimate	4x
SimulaVariable::getRate: estimating initial rate for rootCarbonCosts. This maybe a poor estimate	26x
SoilImpedance::theta2psi: reported soil water content is greater than saturatedWaterContent. Using saturatedWaterContent; please check your parameters.	1325x
Swms3d:: simulating nitrate	1x
Swms3d::getTimeStepSWMS:: wallTime before lastTime, this is bad - ignoring wall time.	16x
WaterUptakeDoussanModel: Can not find solution incrementally, trying direct interpolation.	1x
WaterUptakeDoussanModel: Ignoring node with name growthpoint	135x
WaterUptakeDoussanModel: Root system can not take up enough water at day 2	15x
WaterUptakeDoussanModel: Root system can not take up enough water at day 3	17x
WaterUptakeDoussanModel::build: getting low h(soil), setting lr to 0. Root outside grid?	1675x
WaterUptakeDoussanModel::build: using same matrix	511x
Watflow: Using Free drainage bottom boundary.	1x
Watflow:: using evaporation from environment section, i.e. given, predefined.	1x
Watflow::Surface area water mesh is 484	1x
