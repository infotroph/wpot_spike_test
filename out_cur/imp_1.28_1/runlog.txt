[30;47mRunning OpenSimRoot 
	build on May  7 2020, 
	licensed GPLv3 
	git 67fdb3eda1
[34;47mOpenSimRoot was build on May  7 2020 from git version 67fdb3eda1[30;47m

Trying to load model from file: [32;47m[800C[1DOK[30;47m
[34;47mSeeding random number generator with  946525.[30;47m
[30;47m
Running modules: [30;47mRunning modules: 
[34;47mEnabling Probe All Objects for current the model is faster and more accurate if "carbonAllocation2Shoot" and "plantNutrientUptake" is called first. Enable this in parameter section to get rid of this message.[30;47m

[34;47mGenerateRoot: using root class specific template for hypocotyl[30;47m

[34;47mRootGrowthDirection: timeScalingFactor not found. Defaulting to 5.[30;47m

[34;47mIntegrationBase:: setting defaultSpatialIntegrationLength to 1.000000.[30;47m

[34;47mImpedanceGao: no call pointer passed so can't look up water content. Returning 0[30;47m

[34;47mLeafAreaIndex: using mean leaf area based on areaPerPlant.[30;47m

[34;47mConstantRootGrowth: growthRateMultiplier found[30;47m

[34;47mWatflow:: using evaporation from environment section, i.e. given, predefined.[30;47m

[34;47mWatflow: Using Free drainage bottom boundary.[30;47m

[34;47mWatflow::Surface area water mesh is 484[30;47m

[34;47mPlantTotal: Using initial value for plantNutrientUptake from parameter section[30;47m

[34;47mSwms3d:: simulating nitrate[30;47m

[34;47mRootDiameter: using length - diameter relation. e.g. longer laterals are thicker[30;47m

[34;47mRootDiameter: no lengthMultiplier2DiameterMultiplier specified for hypocotyls of imp_1.28[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for carbonAvailableForGrowth. This maybe a poor estimate[30;47m

[34;47mSimulaExternal:: locked, and it has no predictor. This is likely to cause extrapolation warnings. [30;47m

[34;47mWaterUptakeDoussanModel::build: using same matrix[30;47m

[34;47mSwms3d::getTimeStepSWMS:: wallTime before lastTime, this is bad - ignoring wall time.[30;47m

[34;47mRootDataPoints: refusing to create points based on estimated data, but the data is before t.[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for carbonAllocation2Shoot. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for plantCarbonIncome. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for rootCarbonCosts. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for carbonReserves. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for carbonAllocation2Leafs. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for carbonAllocation2Stems. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for leafArea. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for reserves. This maybe a poor estimate[30;47m

[34;47mSimulaVariable::getRate: estimating initial rate for rootCarbonCostOfNutrientUptake. This maybe a poor estimate[30;47m
[s0.0/5.0 days. Mem 238.0 mB. #obj.=1891 x64b/obj.=16496.6[u
[34;47mPlantCarbonBalance: balance error at +0.10 days: error (%) tot=+2.5264 root=+2.5264 alloc=+0.0000 for maize[30;47m
[s0.1/5.0 days. Mem 238.0 mB. #obj.=1891 x64b/obj.=16496.6[u[s0.2/5.0 days. Mem 238.0 mB. #obj.=1935 x64b/obj.=16121.5[u
[34;47mPlantCarbonBalance: balance error at +0.30 days: error (%) tot=+0.8023 root=+0.8023 alloc=+0.0000 for maize[30;47m
[s0.3/5.0 days. Mem 239.0 mB. #obj.=1935 x64b/obj.=16189.3[u
[34;47mSimulaTable:: Extrapolation at end of table hydraulicHeadAtRootSurface[30;47m

[34;47mSimulaTable:: Extrapolation at end of table volumetricWaterContentAtTheRootSurface[30;47m

[34;47mRootSegmentLength: length based on estimated code, but this should not be the case as nodes are not created based on estimates. (2)[30;47m

[34;47mRootSegmentLength: length based on estimated code, but this should not be the case as nodes are not created based on estimates.[30;47m

[34;47mWaterUptakeDoussanModel: Ignoring node with name growthpoint[30;47m
[s0.4/5.0 days. Mem 239.0 mB. #obj.=2627 x64b/obj.=11924.7[u[s0.5/5.0 days. Mem 239.0 mB. #obj.=2627 x64b/obj.=11924.7[u[s0.6/5.0 days. Mem 239.0 mB. #obj.=2671 x64b/obj.=11728.3[u[s0.7/5.0 days. Mem 239.0 mB. #obj.=2671 x64b/obj.=11728.3[u[s0.8/5.0 days. Mem 239.0 mB. #obj.=2715 x64b/obj.=11538.2[u[s0.9/5.0 days. Mem 239.0 mB. #obj.=2803 x64b/obj.=11176.0[u[s1.0/5.0 days. Mem 239.0 mB. #obj.=2803 x64b/obj.=11176.0[u[s1.0/5.0 days. Mem 239.0 mB. #obj.=2803 x64b/obj.=11176.0[u[s1.1/5.0 days. Mem 239.0 mB. #obj.=2847 x64b/obj.=11003.2[u[s1.2/5.0 days. Mem 239.0 mB. #obj.=2847 x64b/obj.=11003.2[u[s1.3/5.0 days. Mem 239.0 mB. #obj.=2891 x64b/obj.=10835.8[u[s1.4/5.0 days. Mem 240.0 mB. #obj.=2891 x64b/obj.=10881.1[u[s1.5/5.0 days. Mem 240.0 mB. #obj.=2979 x64b/obj.=10559.7[u
[34;47mWaterUptakeDoussanModel::build: getting low h(soil), setting lr to 0. Root outside grid?[30;47m
[s1.6/5.0 days. Mem 240.0 mB. #obj.=2979 x64b/obj.=10559.7[u[s1.7/5.0 days. Mem 240.0 mB. #obj.=3023 x64b/obj.=10406.0[u[s1.8/5.0 days. Mem 240.0 mB. #obj.=3023 x64b/obj.=10406.0[u
[34;47mLeafAreaReductionCoefficient: Reducing leaf area[30;47m
[s1.9/5.0 days. Mem 240.0 mB. #obj.=3111 x64b/obj.=10111.6[u[s2.0/5.0 days. Mem 240.0 mB. #obj.=3111 x64b/obj.=10111.6[u
[34;47mWaterUptakeDoussanModel: Root system can not take up enough water at day 2[30;47m
[s2.0/5.0 days. Mem 240.0 mB. #obj.=3111 x64b/obj.=10111.6[u
[34;47mPlantCarbonBalance: balance error at +2.10 days: error (%) tot=+0.6913 root=+0.6889 alloc=+0.0000 for maize[30;47m
[s2.1/5.0 days. Mem 240.0 mB. #obj.=3507 x64b/obj.=8969.9[u[s2.2/5.0 days. Mem 240.0 mB. #obj.=3507 x64b/obj.=8969.9[u
[34;47mWaterUptakeDoussanModel: Root system can not take up enough water at day 3[30;47m

[34;47mPlantCarbonBalance: balance error at +2.30 days: error (%) tot=+0.8237 root=+0.8192 alloc=+0.0000 for maize[30;47m
[s2.3/5.0 days. Mem 240.0 mB. #obj.=3815 x64b/obj.=8245.7[u[s2.4/5.0 days. Mem 240.0 mB. #obj.=3815 x64b/obj.=8245.7[u
[34;47mPlantCarbonBalance: balance error at +2.50 days: error (%) tot=+0.5889 root=+0.5836 alloc=+0.0000 for maize[30;47m
[s2.5/5.0 days. Mem 240.0 mB. #obj.=4415 x64b/obj.=7125.1[u[s2.6/5.0 days. Mem 240.0 mB. #obj.=4415 x64b/obj.=7125.1[u[s2.7/5.0 days. Mem 241.0 mB. #obj.=5015 x64b/obj.=6298.8[u[s2.8/5.0 days. Mem 241.0 mB. #obj.=5015 x64b/obj.=6298.8[u
[34;47mSoilImpedance::theta2psi: reported soil water content is greater than saturatedWaterContent. Using saturatedWaterContent; please check your parameters.[30;47m
[s2.9/5.0 days. Mem 241.0 mB. #obj.=5995 x64b/obj.=5269.1[u[s3.0/5.0 days. Mem 241.0 mB. #obj.=5995 x64b/obj.=5269.1[u[s3.0/5.0 days. Mem 241.0 mB. #obj.=5995 x64b/obj.=5269.1[u[s3.1/5.0 days. Mem 241.0 mB. #obj.=6483 x64b/obj.=4872.5[u[s3.2/5.0 days. Mem 241.0 mB. #obj.=6483 x64b/obj.=4872.5[u[s3.3/5.0 days. Mem 241.0 mB. #obj.=6971 x64b/obj.=4531.4[u
[34;47mWaterUptakeDoussanModel: Can not find solution incrementally, trying direct interpolation.[30;47m
[s3.4/5.0 days. Mem 241.0 mB. #obj.=6971 x64b/obj.=4531.4[u[s3.5/5.0 days. Mem 242.0 mB. #obj.=7459 x64b/obj.=4252.5[u[s3.6/5.0 days. Mem 242.0 mB. #obj.=7459 x64b/obj.=4252.5[u[s3.7/5.0 days. Mem 242.0 mB. #obj.=7879 x64b/obj.=4025.8[u[s3.8/5.0 days. Mem 242.0 mB. #obj.=7879 x64b/obj.=4025.8[u
[34;47mWaterUptakeDoussanModel: Root system can not take up enough water at day 4[30;47m
[s3.9/5.0 days. Mem 242.0 mB. #obj.=9487 x64b/obj.=3343.5[u[s4.0/5.0 days. Mem 242.0 mB. #obj.=9487 x64b/obj.=3343.5[u[s4.0/5.0 days. Mem 242.0 mB. #obj.=9487 x64b/obj.=3343.5[u[s4.1/5.0 days. Mem 243.0 mB. #obj.=11991 x64b/obj.=2656.2[u[s4.2/5.0 days. Mem 243.0 mB. #obj.=11991 x64b/obj.=2656.2[u[s4.3/5.0 days. Mem 244.0 mB. #obj.=14383 x64b/obj.=2223.6[u[s4.4/5.0 days. Mem 244.0 mB. #obj.=14383 x64b/obj.=2223.6[u[s4.5/5.0 days. Mem 245.0 mB. #obj.=17223 x64b/obj.=1864.5[u[s4.6/5.0 days. Mem 245.0 mB. #obj.=17223 x64b/obj.=1864.5[u[s4.7/5.0 days. Mem 246.0 mB. #obj.=20847 x64b/obj.=1546.7[u[s4.8/5.0 days. Mem 246.0 mB. #obj.=20847 x64b/obj.=1546.7[u[s4.9/5.0 days. Mem 247.0 mB. #obj.=23911 x64b/obj.=1354.0[u[s5.0/5.0 days. Mem 247.0 mB. #obj.=23911 x64b/obj.=1354.0[u
[34;47mAt 5 days mem usage was 247 mB.[30;47m
[32;47m[800C[1DOK[30;47m
Finalizing output:[32;47m[800C[1DOK[30;47m[34;47m
THE MODEL PUT OUT WARNINGS:
At 5 days mem usage was 247 mB.[800C[7D1x
ConstantRootGrowth: growthRateMultiplier found[800C[7D155x
Enabling Probe All Objects for current the model is faster and more accurate if "carbonAllocation2Shoot" and "plantNutrientUptake" is called first. Enable this in parameter section to get rid of this message.[800C[7D1x
GenerateRoot: using root class specific template for hypocotyl[800C[7D1x
ImpedanceGao: no call pointer passed so can't look up water content. Returning 0[800C[7D51x
IntegrationBase:: setting defaultSpatialIntegrationLength to 1.000000.[800C[7D1x
LeafAreaIndex: using mean leaf area based on areaPerPlant.[800C[7D1x
LeafAreaReductionCoefficient: Reducing leaf area[800C[7D236x
OpenSimRoot was build on May  7 2020 from git version 67fdb3eda1[800C[7D1x
PlantCarbonBalance: balance error at +0.10 days: error (%) tot=+2.5264 root=+2.5264 alloc=+0.0000 for maize[800C[7D1x
PlantCarbonBalance: balance error at +0.30 days: error (%) tot=+0.8023 root=+0.8023 alloc=+0.0000 for maize[800C[7D1x
PlantCarbonBalance: balance error at +2.10 days: error (%) tot=+0.6913 root=+0.6889 alloc=+0.0000 for maize[800C[7D1x
PlantCarbonBalance: balance error at +2.30 days: error (%) tot=+0.8237 root=+0.8192 alloc=+0.0000 for maize[800C[7D1x
PlantCarbonBalance: balance error at +2.50 days: error (%) tot=+0.5889 root=+0.5836 alloc=+0.0000 for maize[800C[7D1x
PlantTotal: Using initial value for plantNutrientUptake from parameter section[800C[7D1x
RootDataPoints: refusing to create points based on estimated data, but the data is before t.[800C[7D32494x
RootDiameter: no lengthMultiplier2DiameterMultiplier specified for hypocotyls of imp_1.28[800C[7D2x
RootDiameter: using length - diameter relation. e.g. longer laterals are thicker[800C[7D310x
RootGrowthDirection: timeScalingFactor not found. Defaulting to 5.[800C[7D1x
RootSegmentLength: length based on estimated code, but this should not be the case as nodes are not created based on estimates.[800C[7D19x
RootSegmentLength: length based on estimated code, but this should not be the case as nodes are not created based on estimates. (2)[800C[7D19x
Seeding random number generator with  946525.[800C[7D1x
SimulaExternal:: locked, and it has no predictor. This is likely to cause extrapolation warnings. [800C[7D69850x
SimulaTable:: Extrapolation at end of table hydraulicHeadAtRootSurface[800C[7D2886x
SimulaTable:: Extrapolation at end of table volumetricWaterContentAtTheRootSurface[800C[7D36618x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Leafs. This maybe a poor estimate[800C[7D4x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Shoot. This maybe a poor estimate[800C[7D28x
SimulaVariable::getRate: estimating initial rate for carbonAllocation2Stems. This maybe a poor estimate[800C[7D4x
SimulaVariable::getRate: estimating initial rate for carbonAvailableForGrowth. This maybe a poor estimate[800C[7D82x
SimulaVariable::getRate: estimating initial rate for carbonReserves. This maybe a poor estimate[800C[7D4x
SimulaVariable::getRate: estimating initial rate for leafArea. This maybe a poor estimate[800C[7D4x
SimulaVariable::getRate: estimating initial rate for plantCarbonIncome. This maybe a poor estimate[800C[7D10x
SimulaVariable::getRate: estimating initial rate for reserves. This maybe a poor estimate[800C[7D4x
SimulaVariable::getRate: estimating initial rate for rootCarbonCostOfNutrientUptake. This maybe a poor estimate[800C[7D4x
SimulaVariable::getRate: estimating initial rate for rootCarbonCosts. This maybe a poor estimate[800C[7D26x
SoilImpedance::theta2psi: reported soil water content is greater than saturatedWaterContent. Using saturatedWaterContent; please check your parameters.[800C[7D1350x
Swms3d:: simulating nitrate[800C[7D1x
Swms3d::getTimeStepSWMS:: wallTime before lastTime, this is bad - ignoring wall time.[800C[7D16x
WaterUptakeDoussanModel: Can not find solution incrementally, trying direct interpolation.[800C[7D1x
WaterUptakeDoussanModel: Ignoring node with name growthpoint[800C[7D153x
WaterUptakeDoussanModel: Root system can not take up enough water at day 2[800C[7D11x
WaterUptakeDoussanModel: Root system can not take up enough water at day 3[800C[7D17x
WaterUptakeDoussanModel: Root system can not take up enough water at day 4[800C[7D6x
WaterUptakeDoussanModel::build: getting low h(soil), setting lr to 0. Root outside grid?[800C[7D1454x
WaterUptakeDoussanModel::build: using same matrix[800C[7D510x
Watflow: Using Free drainage bottom boundary.[800C[7D1x
Watflow:: using evaporation from environment section, i.e. given, predefined.[800C[7D1x
Watflow::Surface area water mesh is 484[800C[7D1x[30;47m
Simulation took (hours:minutes:seconds): 0:4:0[0m
